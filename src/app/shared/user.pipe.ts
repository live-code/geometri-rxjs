import { Pipe, PipeTransform } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../model/user";
import {map} from "rxjs";

@Pipe({
  name: 'displayName'
})
export class UserPipe implements PipeTransform {

  constructor(private http: HttpClient) {
  }
  transform(id: number) {
    return this.http.get<User>('https://jsonplaceholder.typicode.com/users/' + id)
      .pipe(
        map(user => `Your Name is ${user.name}`)
      )
  }

}
