import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserPipe } from './user.pipe';
import { SuperHtmlPipe } from './super-html.pipe';



@NgModule({
  declarations: [
    UserPipe,
    SuperHtmlPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    UserPipe,
    SuperHtmlPipe
  ]
})
export class SharedModule { }
