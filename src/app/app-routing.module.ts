import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from "./core/auth/auth.guard";

const routes: Routes = [
  { path: 'page1', canActivate: [AuthGuard], loadChildren: () => import('./features/page1/page1.module').then(m => m.Page1Module) },
  { path: 'page2', loadChildren: () => import('./features/page2/page2.module').then(m => m.Page2Module) },
  { path: 'page3', loadChildren: () => import('./features/page3/page3.module').then(m => m.Page3Module) },
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
