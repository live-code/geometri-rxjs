import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {FormBuilder, FormControl, NonNullableFormBuilder, Validators} from "@angular/forms";
import {AuthService} from "../../core/auth/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  template: `
    <form [formGroup]="form" (submit)="login()">
      <input type="text"  formControlName="username">
      <input type="text" formControlName="password">
      <button type="submit">LOGIN</button>
    </form>
  `,
})
export class LoginComponent  {
  form = this.fb.group({
    username: new FormControl(''),
    password: ['', Validators.required],
  }, { nonNullable: true })

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {}

  login() {
    this.authService.login(
      this.form.value.username,
      this.form.value.password,
    )
      .subscribe(res => {
        this.router.navigateByUrl('page1')
      })

  }
}
