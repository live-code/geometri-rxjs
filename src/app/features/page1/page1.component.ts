import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {concatMap, delay, exhaustMap, fromEvent, map, mergeAll, mergeMap, scan, switchMap, tap} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {FormControl} from "@angular/forms";
import {User} from "../../model/user";

@Component({
  selector: 'app-page1',
  template: `
    <button #btn>NEXT</button>

    <div *ngIf="error">{{error}}</div>

    <pre>{{user | json}}</pre>
  `,
})
export class Page1Component implements AfterViewInit {
  @ViewChild('btn') button!: ElementRef<any>;
  user: User | undefined;
  error: any

  constructor(private http: HttpClient) {}

  ngAfterViewInit() {
    fromEvent(this.button.nativeElement, 'click')
      .pipe(
        scan((count) => count < 10 ? ++count : 1, 0),
        concatMap(index => this.http.get<User>(`https://jsonplaceholder.typicode.com/xxxusers/${index}`))
      )
      .subscribe({
        next: (res) => this.user = res,
        error: (err) => {
          console.log(err)
          this.error = err
        }
      })
  }


}
