import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Page1RoutingModule } from './page1-routing.module';
import { Page1Component } from './page1.component';
import {SharedModule} from "../../shared/shared.module";
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    Page1Component
  ],
  imports: [
    SharedModule,
    ReactiveFormsModule,
    CommonModule,
    Page1RoutingModule
  ]
})
export class Page1Module { }
