import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../../model/user";
import {share} from "rxjs";

@Component({
  selector: 'app-page2',
  template: `
    <p>
      page2 works!
    </p>

    <pre>{{(user$ | async)?.id  }}</pre>
    <pre>{{(user$ | async)?.name  }}</pre>
  `,
  styles: [
  ]
})
export class Page2Component implements OnInit {
  user$ = this.http.get<User>(`https://jsonplaceholder.typicode.com/usersx/1`)
    .pipe(
      share({
        resetOnComplete: false,
        resetOnError: false,
      })
    )

  constructor(private http: HttpClient) {
    setTimeout(() => {
      this.user$.subscribe()
    }, 1000)
  }

  ngOnInit(): void {
  }

}
