import { Component } from '@angular/core';
import {AuthService} from "./core/auth/auth.service";

@Component({
  selector: 'app-root',
  template: `
    <button  routerLink="login">Login</button>
    <button  *appIfLogged routerLink="page1">1</button>
    <button *appIfRoleIs="'admin'" routerLink="page2">2</button>
    <button  routerLink="page3">3</button>
    <button (click)="authService.logout()">lougout</button>


    <div [innerHTML]="'123' | superHtml">xxxxx</div>
    <hr>





    <div *ngIf="visible; else msg">loading</div>

    <ng-template #msg>
      cioa a tutti
    </ng-template>

    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  visible = false;

  constructor(public authService: AuthService) {
  }
}
