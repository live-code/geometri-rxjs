import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';
import {AuthService} from "./auth.service";
import {distinctUntilChanged, filter, tap} from "rxjs";

@Directive({
  selector: '[appIfRoleIs]'
})
export class IfRoleIsDirective {
  @Input() appIfRoleIs: string | undefined

  constructor(
    private authService: AuthService,
    private view: ViewContainerRef,
    private tpl: TemplateRef<any>
  ) {
    this.authService.role$()
      .pipe(
        tap(() =>  view.clear()),
        distinctUntilChanged(),
        filter(role => !!role)
      )
      .subscribe(role => {
        if(role === this.appIfRoleIs)
          view.createEmbeddedView(tpl)
      })
  }

}
