import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {catchError, first, iif, interval, map, mergeMap, Observable, of, retry, throwError} from 'rxjs';
import {AuthService} from "./auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authService.token$()
      .pipe(
        first(),
        mergeMap(tk => {
          return iif(
            () => !!tk,
            next.handle(request.clone({setHeaders: {Authorization: tk!}})),
            next.handle(request)
          )
        }),
        retry({ count: 2, delay: () => interval(1000)}),
        catchError(err => {
          return throwError({...err, pippo: 123})
        })
      )


    /*return this.authService.auth$
      .pipe(
        first(),
        mergeMap(auth => {
          const tk = auth?.token;
          let cloneReq = request;
          if (tk) {
            cloneReq = request.clone({setHeaders: {Authorization: tk}})
          }
          return next.handle(cloneReq)
        })
      )
*/


  }
}

/*


.pipe(
  // retry({ count: 2, delay: () => interval(1000)}),
  catchError(err => {
    return throwError({...err, pippo: 123})
  })
)*/
