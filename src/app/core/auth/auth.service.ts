import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {FormBuilder} from "@angular/forms";
import {Auth} from "../../model/auth";
import {Router} from "@angular/router";
import {BehaviorSubject, map, Observable, share} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // auth: Auth | null = null;
  auth$ = new BehaviorSubject<Auth | null>(null)

  constructor(private http: HttpClient) {}

  login(username: string, password: string) {

    const req$ = this.http.get<Auth>('http://localhost:3000/login')
      .pipe(
        share()
      )

    req$.subscribe({
      next: (res) => {
        // this.auth = res;
        this.auth$.next(res);
      },
      error: err => {
      }
    })
    return req$;
  }

  logout() {
    this.auth$.next(null)
  }

  isLogged$(): Observable<boolean> {
    return this.auth$
      .pipe(
        map(auth => !!auth)
      )
  }

  token$(): Observable<string | undefined> {
    return this.auth$
      .pipe(
        map(auth => auth?.token)
      )
  }

  role$(): Observable<string | undefined> {
    return this.auth$
      .pipe(
        map(auth => auth?.role)
      )
  }

}
