import {Directive, TemplateRef, ViewContainerRef} from '@angular/core';
import {AuthService} from "./auth.service";
import {distinctUntilChanged} from "rxjs";

@Directive({
  selector: '[appIfLogged]'
})
export class IfLoggedDirective {

  constructor(
    private authService: AuthService,
    private view: ViewContainerRef,
    private tpl: TemplateRef<any>
  ) {
    this.authService.isLogged$()
      .pipe(
        distinctUntilChanged()
      )
      .subscribe(isLogged => {
        if(isLogged)
          view.createEmbeddedView(tpl)
        else
          view.clear()
      })
  }

}
